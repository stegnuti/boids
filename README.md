My Unity C# implementation of a distributed behavioural model inspired by Craig Reynlods' "Flocks, Herds, and Schools:
A Distributed Behavioral Model", published in 1986.